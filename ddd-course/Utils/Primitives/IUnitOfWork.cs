﻿using Microsoft.EntityFrameworkCore.Storage;

namespace Primitives
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default);
    }
}