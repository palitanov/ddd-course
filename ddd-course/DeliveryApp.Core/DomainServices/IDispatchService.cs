using CSharpFunctionalExtensions;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.OrderAggregate;
using Primitives;

namespace DeliveryApp.Core.DomainServices;

public interface IDispatchService
{
    Result<Courier, Error> Dispatch(Order order, List<Courier> couriers);
}