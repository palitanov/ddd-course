using CSharpFunctionalExtensions;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.OrderAggregate;
using Primitives;

namespace DeliveryApp.Core.DomainServices;

public class DispatchService : IDispatchService
{
    public static class Errors
    {
        public static Error ThereAreNoCouriersForDispatching()
        {
            return new($"there.are.no.couriers.for.dispatching", "Нет свободных курьеров");
        }
        
        public static Error ThereAreNoCouriersWithSuitableTransport()
        {
            return new($"there.are.no.couriers.with.suitable.transport", "Нет курьеров с подходящим транспортом");
        }
        
        public static Error TimeCalculationError()
        {
            return new($"time.calculation.error", "Ошибка подсчета времени");
        }
    }

    public Result<Courier, Error> Dispatch(Order order, List<Courier> couriers)
    {
        if (!couriers.Any())
            return Errors.ThereAreNoCouriersForDispatching();
        
        var canCarryCouriers = couriers.Where(x=>
            {
                var canCarryWeight = x.Transport?.CanCarryWeight(order.Weight) ?? false;
                return canCarryWeight.IsSuccess && canCarryWeight.Value;
            }).ToList();
        
        if (!canCarryCouriers.Any())
            return Errors.ThereAreNoCouriersWithSuitableTransport();

        var scoringList = canCarryCouriers.Select(x => new { courier = x, score = x.CalculateTimeToPoint(order.Location)}).Where(y => y.score.IsSuccess).ToList();
        
        if (!scoringList.Any())
            return Errors.TimeCalculationError();

        var result = scoringList.MinBy(x=>x.score.Value);
        return result.courier;
    }
}