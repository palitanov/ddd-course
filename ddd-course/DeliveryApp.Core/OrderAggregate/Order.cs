using CSharpFunctionalExtensions;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.OrderAggregate.DomainEvents;
using DeliveryApp.Core.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.OrderAggregate;

public class Order : Aggregate
{
    public static class Errors
    {
        public static Error OrderHasAlreadyBeenCompleted()
        {
            return new($"{nameof(Order).ToLowerInvariant()}.has.already.been.completed", "Заказ уже выполнен");
        }
        
        public static Error OrderStatusShouldBeAssigned()
        {
            return new($"{nameof(Order).ToLowerInvariant()}.status.should.be.assigned", "Заказ не назначен");
        }
    }
    
    protected Order(Guid id, Location location, Weight weight, OrderStatus status)
        : base(id)
    {
        Location = location;
        Weight = weight;
        SetOrderStatus(status);
    }

    protected Order()
    {
    }

    public Guid CourierId { get; protected set; }

    public Location Location  { get; }
    
    public Weight  Weight   { get; }
    
    public OrderStatus  Status   { get; protected set; }
    
    public static Result<Order, Error> Create(Guid id, Location location, Weight weight)
    {
        if (id == Guid.Empty) return GeneralErrors.ValueIsRequired(nameof(id));
        if (location==null) return GeneralErrors.ValueIsRequired(nameof(location));
        if (weight==null) return GeneralErrors.ValueIsRequired(nameof(weight));
        if (!location.IsValid()) return GeneralErrors.ValueIsInvalid(nameof(location));
        if (!weight.IsValid()) return GeneralErrors.ValueIsInvalid(nameof(weight));
        
        return new Order(id, location, weight, OrderStatus.Created);
    }
    
    public Result<object, Error> Complete()
    {
        if (Status == OrderStatus.Completed) return Errors.OrderHasAlreadyBeenCompleted();
        if (Status != OrderStatus.Assigned) return Errors.OrderStatusShouldBeAssigned();
        
        SetOrderStatus(OrderStatus.Completed);
        return new object();
    }
    
    public Result<object, Error> Assign(Courier courier)
    {
        if (courier==null) return GeneralErrors.ValueIsRequired(nameof(courier));
        if (Status == OrderStatus.Completed) return Errors.OrderHasAlreadyBeenCompleted();

        var courierInWorkResult = courier.InWork();
        if (courierInWorkResult.IsFailure) return courierInWorkResult.Error;

        CourierId = courier.Id;
        SetOrderStatus(OrderStatus.Assigned);
        return new object();
    }
    
    private void SetOrderStatus(OrderStatus status)
    {
        if (Status==status)
            return;
        
        Status = status;
        RaiseDomainEvent(new OrderStatusChangedDomainEvent(Id, Status));
        Console.WriteLine($"Order {Id} {Status.Value}");
    }
}