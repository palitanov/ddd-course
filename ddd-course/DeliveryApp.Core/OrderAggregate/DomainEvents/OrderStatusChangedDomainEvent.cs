using Primitives;

namespace DeliveryApp.Core.OrderAggregate.DomainEvents
{
    public sealed record OrderStatusChangedDomainEvent(Guid BasketId, OrderStatus OrderStatus) : DomainEvent;
}

