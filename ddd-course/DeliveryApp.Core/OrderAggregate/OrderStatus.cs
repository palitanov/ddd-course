using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.OrderAggregate;

public class OrderStatus : ValueObject
{
    public static readonly OrderStatus Created = new(nameof(Created).ToLowerInvariant());
    public static readonly OrderStatus Assigned = new(nameof(Assigned).ToLowerInvariant());
    public static readonly OrderStatus Completed = new(nameof(Completed).ToLowerInvariant());

    protected OrderStatus()
    {
    }

    protected OrderStatus(string value)
    {
        Value = value;
    }

    public string Value { get; init; }

    public static Result<OrderStatus, Error> Create(string input)
    {
        if (string.IsNullOrWhiteSpace(input)) return GeneralErrors.ValueIsRequired(nameof(input));
        return new OrderStatus(input);
    }

    public static Result<OrderStatus, Error> FromName(string name)
    {
        var status = List()
            .SingleOrDefault(s => string.Equals(s.Value, name, StringComparison.CurrentCultureIgnoreCase));
        if (status == null) return Errors.StatusIsWrong();
        return status;
    }

    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }

    public static IEnumerable<OrderStatus> List()
    {
        yield return Created;
        yield return Assigned;
        yield return Completed;
    }

    public static class Errors
    {
        public static Error StatusIsWrong()
        {
            return new Error($"{nameof(OrderStatus).ToLowerInvariant()}.is.wrong",
                $"Invalid value. Acceptable values: {nameof(OrderStatus).ToLowerInvariant()}: {string.Join(",", List().Select(s => s.Value))}");
        }
    }
}