using CSharpFunctionalExtensions;
using DeliveryApp.Core.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.CourierAggregate;

public class Transport : Entity<int>
{
    public static readonly Transport Pedestrian = new Transport(1, "pedestrian", 1, Weight.Create(1).Value);
    public static readonly Transport Bicycle  = new Transport(2, "bicycle", 2, Weight.Create(4).Value);
    public static readonly Transport Scooter  = new Transport(3, "scooter", 3, Weight.Create(6).Value);
    public static readonly Transport Car  = new Transport(4, "car", 4, Weight.Create(8).Value);
    
    public static class Errors
    {
        public static Error TransportNameIsWrong()
        {
            return new Error($"{nameof(Transport).ToLowerInvariant()}.name.is.wrong",
                $"Invalid value. Acceptable values: {nameof(Transport).ToLowerInvariant()}: {string.Join(",", List().Select(s => s.Name))}");
        }

        public static Error TransportIdIsWrong()
        {
            return new Error($"{nameof(Transport).ToLowerInvariant()}.id.is.wrong",
                $"Invalid value. Acceptable values: {nameof(Transport).ToLowerInvariant()}: {string.Join(",", List().Select(s => s.Id))}");
        }
    }
    
    protected Transport(int id, string name, int speed, Weight capacity)
        : base(id)
    {
        Name = name;
        Speed = speed;
        Capacity = capacity;
    }

    protected Transport()
    {
    }

    public string Name { get; protected set; }
    
    public int Speed  { get; protected set; }
    
    public Weight  Capacity   { get; protected set; }

    public Result<bool, Error> CanCarryWeight(Weight weight)
    {
        if (weight == null) return GeneralErrors.ValueIsRequired(nameof(weight));
        return Capacity >= weight;
    }
    
    public static Result<Transport, Error> FromName(string name)
    {
        var transport = List()
            .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
        if (transport == null) return Errors.TransportNameIsWrong();
        return transport;
    }

    public static Result<Transport, Error> FromId(int id)
    {
        var transport = List().FirstOrDefault(x=>x.Id==id);
        if (transport == null) return Errors.TransportIdIsWrong();
        return transport;
    }
    
    public static IEnumerable<Transport> List()
    {
        yield return Pedestrian;
        yield return Bicycle;
        yield return Scooter;
        yield return Car;
    }
}