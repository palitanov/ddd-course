using CSharpFunctionalExtensions;
using DeliveryApp.Core.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.CourierAggregate;

public class Courier : Aggregate
{
    public static class Errors
    {
        public static Error CourierIsAlreadyBusy()
        {
            return new($"{nameof(Courier).ToLowerInvariant()}.is.already.busy", "Курьер уже занят");
        }
        
        public static Error CourierIsBusy()
        {
            return new($"{nameof(Courier).ToLowerInvariant()}.is.busy", "Курьер занят");
        }
        
        public static Error CourierIsNotReady()
        {
            return new($"{nameof(Courier).ToLowerInvariant()}.is.not.ready", "Курьер не готов к работе");
        }
    }
    
    protected Courier(string name, Transport transport, Location location, CourierStatus status):base(Guid.NewGuid())
    {
        Name = name;
        Transport = transport;
        Location = location;
        Status = status;
    }

    protected Courier():base(Guid.NewGuid())
    {
    }

    public string Name { get; protected set; }
    
    public Transport Transport  { get; protected set; }
    
    public Location Location  { get; protected set; }
    
    public CourierStatus Status  { get; private set; }
    
    public static Result<Courier, Error>Create(string name, Transport transport)
    {
        if (string.IsNullOrWhiteSpace(name)) return GeneralErrors.ValueIsRequired(nameof(name));
        if (transport==null) return GeneralErrors.ValueIsRequired(nameof(transport));

        return new Courier(name, transport, Location.CreateDefault(), CourierStatus.NotAvailable);
    }
    
    public Result<bool, Error> Move(Location orderLocation)
    {
        bool orderLocationHasBeenReached = false;
        if (orderLocation == null) return GeneralErrors.ValueIsRequired(nameof(orderLocation));
        if (!orderLocation.IsValid()) return GeneralErrors.ValueIsInvalid(nameof(orderLocation));
        
        int directionX = (orderLocation.X > Location.X) ? 1 : -1;
        int directionY = (orderLocation.Y > Location.Y) ? 1 : -1;
        
        var steps = Transport.Speed;
        var locationX = Location.X;
        var locationY = Location.Y;
        while (steps > 0)
        {
            if (locationX != orderLocation.X)
            {
                locationX = ValidateCoordinate(locationX+directionX);
            }
            else if (locationY != orderLocation.Y)
            {
                locationY = ValidateCoordinate(locationY+directionY);
            }
            steps--;
            if (locationX==orderLocation.X && locationY==orderLocation.Y)
            {
                orderLocationHasBeenReached = true;
                break;
            }
        }
        var result = Location.Create(locationX, locationY);
        if (result.IsFailure) return result.Error;
        Location = result.Value;
        Console.WriteLine($"{Name} {Id} moved to {Location}");
        return orderLocationHasBeenReached;
    }

    private int ValidateCoordinate(int coordinate)
    {
        if (coordinate < Location.MinValue) coordinate = Location.MinValue;
        if (coordinate > Location.MaxValue) coordinate = Location.MaxValue;
        return coordinate;
    }
    
    public void OrderComplete()
    {
        SetStatus(CourierStatus.Ready);
    }

    public Result<object, Error> StartWork()
    {
        if (Status == CourierStatus.Busy) return Errors.CourierIsAlreadyBusy();
        SetStatus(Status = CourierStatus.Ready);
        return new object();
    }
    
    public Result<object, Error> StopWork()
    {
        if (Status == CourierStatus.Busy) return Errors.CourierIsBusy();
        SetStatus(CourierStatus.NotAvailable);
        return new object();
    }
    
    public Result<object, Error> InWork()
    {
        if (Status != CourierStatus.Ready) return Errors.CourierIsNotReady();
        SetStatus(CourierStatus.Busy);
        return new object();
    }
    
    public Result<double, Error> CalculateTimeToPoint(Location location)
    {
        if (location == null) return GeneralErrors.ValueIsRequired(nameof(location));
        if (!location.IsValid()) return GeneralErrors.ValueIsInvalid(nameof(location));
        
        var distanceResult = Location.DistanceTo(location);
        if (distanceResult.IsFailure) return distanceResult.Error;
        
        return (double)distanceResult.Value/Transport.Speed;
    }
    
    public override string ToString()
    {
        return $"Id: {Id}, Name: {Name}, Transport: {Transport.Name}, Location: {Location}, Status: {Status?.Value ?? "null"}";
    }

    private void SetStatus(CourierStatus status)
    {
        if (Status==status)
            return;

        Status = status;
        Console.WriteLine($"{Name} {Id} {Status.Value}");
    }
}