using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.CourierAggregate;

public class CourierStatus : ValueObject
{
    public static CourierStatus NotAvailable => new(nameof(NotAvailable).ToLowerInvariant());
    public static CourierStatus Ready => new(nameof(Ready).ToLowerInvariant());
    public static CourierStatus Busy => new(nameof(Busy).ToLowerInvariant());

    protected CourierStatus()
    {
    }

    protected CourierStatus(string value)
    {
        Value = value;
    }

    public string Value { get; }

    public static Result<CourierStatus, Error> Create(string input)
    {
        if (string.IsNullOrWhiteSpace(input)) return GeneralErrors.ValueIsRequired(nameof(input));
        return new CourierStatus(input);
    }

    public static Result<CourierStatus, Error> FromName(string name)
    {
        var status = List()
            .SingleOrDefault(s => string.Equals(s.Value, name, StringComparison.CurrentCultureIgnoreCase));
        if (status == null) return Errors.StatusIsWrong();
        return status;
    }

    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }

    public static IEnumerable<CourierStatus> List()
    {
        yield return NotAvailable;
        yield return Ready;
        yield return Busy;
    }

    public static class Errors
    {
        public static Error StatusIsWrong()
        {
            return new Error($"{nameof(CourierStatus).ToLowerInvariant()}.is.wrong",
                $"Invalid value. Acceptable values: {nameof(CourierStatus).ToLowerInvariant()}: {string.Join(",", List().Select(s => s.Value))}");
        }
    }
}