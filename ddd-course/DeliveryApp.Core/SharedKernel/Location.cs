using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.SharedKernel;

public class Location : ValueObject
{
    public const int MinValue = 1;
    public const int MaxValue = 10;

    public static readonly Location Default = Location.Create(MinValue, MinValue).Value;
    
    private Location(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; private set; }

    public int Y { get; private set; }

    public static Result<Location, Error> Create(int x, int y)
    {
        if (x < MinValue || x > MaxValue) return Errors.ValueOutOfRange(nameof(x));
        if (y < MinValue || y > MaxValue) return Errors.ValueOutOfRange(nameof(y));

        return new Location(x, y);
    }

    public Result<int, Error> DistanceTo(Location targetLocation)
    {
        if (targetLocation == null) return GeneralErrors.ValueIsRequired(nameof(targetLocation));
        if (!targetLocation.IsValid()) return GeneralErrors.ValueIsInvalid(nameof(targetLocation));
        return Math.Abs(targetLocation.X - X) + Math.Abs(targetLocation.Y - Y);
    }

    public bool IsValid()
    {
        return X is >= MinValue and <= MaxValue && Y is >= MinValue and <= MaxValue;
    }
    
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return X;
        yield return Y;
    }

    public static class Errors
    {
        public static Error ValueOutOfRange(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentException(name);
            return new Error("out.of.range", $"{name} out of range. Permissible range {MinValue}-{MaxValue}");
        }
    }

    public override string ToString()
    {
        return $"X: {X}, Y: {Y}";
    }

    public static Location CreateDefault()
    {
        return Create(MinValue, MinValue).Value;
    }
    
    public static Result<Location, Error> CreateRandom()
    {
        var rnd = new Random();
        var x = rnd.Next(MinValue, MaxValue);
        var y = rnd.Next(MinValue, MaxValue);
        var location = new Location(x, y);
        return location;
    }
}