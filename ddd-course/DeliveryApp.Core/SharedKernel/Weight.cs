using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.SharedKernel;

public class Weight : ValueObject
{
    private Weight(int value)
    {
        Value = value;
    }

    public int Value { get; }

    public static Result<Weight, Error> Create(int value)
    {
        if (value <= 0) return GeneralErrors.ValueIsInvalid(nameof(value));
        return new Weight(value);
    }

    public static bool operator >(Weight first, Weight second)
    {
        var result = first.Value > second.Value;
        return result;
    }

    public static bool operator <(Weight first, Weight second)
    {
        var result = first.Value < second.Value;
        return result;
    }
    
    public static bool operator <=(Weight first, Weight second)
    {
        var result = first.Value <= second.Value;
        return result;
    }
    
    public static bool operator >=(Weight first, Weight second)
    {
        var result = first.Value >= second.Value;
        return result;
    }

    public bool IsValid()
    {
        return Value > 0;
    }
    
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }
}