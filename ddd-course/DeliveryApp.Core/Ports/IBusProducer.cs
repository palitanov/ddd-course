using DeliveryApp.Core.OrderAggregate.DomainEvents;

namespace DeliveryApp.Core.Ports;

public interface IBusProducer
{
    Task Publish(OrderStatusChangedDomainEvent notification, CancellationToken cancellationToken);
}