using DeliveryApp.Core.CourierAggregate;
using Primitives;

namespace DeliveryApp.Core.Ports;

public interface ICourierRepository: IRepository<Courier>
{
    Courier Add(Courier courier);
    void Update(Courier courier);
    Task<Courier> GetAsync(Guid courierId);
    IEnumerable<Courier> GetAllReady();
    IEnumerable<Courier> GetAllBusy();
}