using CSharpFunctionalExtensions;
using DeliveryApp.Core.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Ports;

public interface IGeoClient
{
    Task<Result<Location, Error>> GetGeolocationAsync(string address, CancellationToken cancellationToken);
}