using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Core.Ports;
using DeliveryApp.Core.SharedKernel;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;

public class Handler : IRequestHandler<Command, bool>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IGeoClient _geoClient;

    public Handler(IOrderRepository orderRepository, IUnitOfWork unitOfWork, IGeoClient geoClient)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentException(nameof(unitOfWork));
        _geoClient = geoClient;
    }

    public async Task<bool> Handle(Command command, CancellationToken cancellationToken)
    {
        var order = await _orderRepository.GetAsync(command.BasketId);
        if (order != null) return false;

        var createWeightResult = Weight.Create(command.Weight);
        if (createWeightResult.IsFailure) return false;
        
        var geolocationResult = await _geoClient.GetGeolocationAsync(command.Address, cancellationToken);
        if (geolocationResult.IsFailure) return false;

        var createOrderResult = Order.Create(command.BasketId, geolocationResult.Value, createWeightResult.Value);
        if (createOrderResult.IsFailure) return false;

        _orderRepository.Add(createOrderResult.Value);
        
        return await _unitOfWork.SaveEntitiesAsync(cancellationToken);
    }
}