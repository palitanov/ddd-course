using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;

public class Command : IRequest<bool>
{
    public Command(Guid basketId, string address, int weight)
    {
        if (basketId == Guid.Empty) throw new ArgumentException(nameof(basketId));
        if (string.IsNullOrWhiteSpace(address)) throw new ArgumentException(nameof(address));
        if (weight<=0) throw new ArgumentException(nameof(weight));
        
        BasketId = basketId;
        Address = address;
        Weight = weight;
    }

    private Command()
    {
    }

    public Guid BasketId { get; }
    
    public string Address { get; }
    
    public int Weight { get;  }
}