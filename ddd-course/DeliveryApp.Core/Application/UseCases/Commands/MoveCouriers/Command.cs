using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.MoveCouriers;

public class Command: IRequest<bool>
{
    public Command()
    {
    }
}