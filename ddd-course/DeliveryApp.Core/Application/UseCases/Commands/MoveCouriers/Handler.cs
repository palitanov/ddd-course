using DeliveryApp.Core.Ports;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.MoveCouriers;

public class Handler: IRequestHandler<Command, bool>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ICourierRepository _courierRepository;
    private readonly IOrderRepository _orderRepository;

    public Handler(ICourierRepository courierRepository, IOrderRepository orderRepository, IUnitOfWork unitOfWork)
    {
        _courierRepository = courierRepository ?? throw new ArgumentException(nameof(courierRepository));
        _orderRepository = orderRepository ?? throw new ArgumentException(nameof(orderRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentException(nameof(unitOfWork));
    }

    public async Task<bool> Handle(Command request, CancellationToken cancellationToken)
    {
        var assignedOrders = _orderRepository.GetAllAssigned().ToList();
        var busyCouriers = _courierRepository.GetAllBusy().ToList();
        
        if (!assignedOrders.Any() || !busyCouriers.Any())
            return false;
        
        var orderList = from o in assignedOrders
            join c in busyCouriers on o.CourierId equals c.Id
            select (Order: o, Courier: c);
        
        foreach (var (order, courier) in orderList)
        {
            if (courier==null)
                continue;

            var result = courier.Move(order.Location);
            
            if (result.IsFailure)
                continue;

            if (order.Location == courier.Location)
            {
                order.Complete();
                courier.OrderComplete();
            }
            
            _courierRepository.Update(courier);
            _orderRepository.Update(order);
        }
        
        return await _unitOfWork.SaveEntitiesAsync(cancellationToken);
    }
}