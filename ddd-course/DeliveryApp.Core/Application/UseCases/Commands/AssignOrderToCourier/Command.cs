using CSharpFunctionalExtensions;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.AssignOrderToCourier;

public class Command: IRequest<Result<object, Error>>
{
    public Command()
    {
    }
}