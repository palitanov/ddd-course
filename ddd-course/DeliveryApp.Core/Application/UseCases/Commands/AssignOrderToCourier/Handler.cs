using CSharpFunctionalExtensions;
using DeliveryApp.Core.DomainServices;
using DeliveryApp.Core.Ports;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.AssignOrderToCourier;

public class Handler: IRequestHandler<Command, Result<object, Error>>
{
    public static class Errors
    {
        public static Error OrderNotFound()
        {
            return new($"order.not.found", "Заказ не найден");
        }
        
        public static Error DataBaseUpdateError()
        {
            return new($"db.update.error", "Ошибка сохранения данных");
        }
    }
    
    private readonly IOrderRepository _orderRepository;
    private readonly ICourierRepository _courierRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IDispatchService _dispatchService;

    public Handler(IOrderRepository orderRepository, ICourierRepository courierRepository, IUnitOfWork unitOfWork, IDispatchService dispatchService)
    {
        _orderRepository = orderRepository ?? throw new ArgumentException(nameof(orderRepository));
        _courierRepository = courierRepository ?? throw new ArgumentException(nameof(courierRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentException(nameof(unitOfWork));
        _dispatchService = dispatchService;
    }

    public async Task<Result<object, Error>> Handle(Command request, CancellationToken cancellationToken)
    {
        var order = _orderRepository.GetAllNotAssigned().FirstOrDefault();
        if (order == null)
            return Errors.OrderNotFound();

        var couriers = _courierRepository.GetAllReady().ToList();

        var dispatchCourierResult = _dispatchService.Dispatch(order, couriers);

        if (dispatchCourierResult.IsFailure)
            return dispatchCourierResult.Error;
        
        var courier = dispatchCourierResult.Value;
        
        var assignResult = order.Assign(courier);

        if (assignResult.IsFailure)
            return assignResult.Error;
        
        _orderRepository.Update(order);
        _courierRepository.Update(courier);

        var result = await _unitOfWork.SaveEntitiesAsync(cancellationToken);

        return result ? new object() : Errors.DataBaseUpdateError();
    }
}