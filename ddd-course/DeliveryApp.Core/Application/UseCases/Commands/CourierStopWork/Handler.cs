using DeliveryApp.Core.Ports;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.CourierStopWork;

public class Handler: IRequestHandler<Command, bool>
{
    private readonly ICourierRepository _courierRepository;
    private readonly IUnitOfWork _unitOfWork;

    public Handler(ICourierRepository courierRepository, IUnitOfWork unitOfWork)
    {
        _courierRepository = courierRepository ?? throw new ArgumentException(nameof(courierRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentException(nameof(unitOfWork));
    }

    public async Task<bool> Handle(Command command, CancellationToken cancellationToken)
    {
        var courier = await _courierRepository.GetAsync(command.CourierId);
        if (courier == null) return false;

        var result = courier.StopWork();
        if (result.IsFailure) return false;
        
        _courierRepository.Update(courier);
        
        return await _unitOfWork.SaveEntitiesAsync(cancellationToken);
    }
}