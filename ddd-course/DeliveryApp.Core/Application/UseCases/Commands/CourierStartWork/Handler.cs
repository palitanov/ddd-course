using DeliveryApp.Core.Ports;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.CourierStartWork;

public class Handler: IRequestHandler<Command, bool>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ICourierRepository _courierRepository;

    public Handler(ICourierRepository courierRepository, IUnitOfWork unitOfWork)
    {
        _courierRepository = courierRepository ?? throw new ArgumentException(nameof(courierRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentException(nameof(unitOfWork));
    }

    public async Task<bool> Handle(Command command, CancellationToken cancellationToken)
    {
        var courier = await _courierRepository.GetAsync(command.CourierId);
        if (courier == null) return false;

        var result = courier.StartWork();
        if (result.IsFailure) return false;
        
        _courierRepository.Update(courier);
        
        return await _unitOfWork.SaveEntitiesAsync(cancellationToken);
    }
}