using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.CourierStartWork;

public class Command: IRequest<bool>
{
    public Command(Guid courierId)
    {
        if (courierId == Guid.Empty) throw new ArgumentException(nameof(courierId));
        
        CourierId = courierId;
    }

    public Guid CourierId { get; private set; }
}