using Dapper;
using DeliveryApp.Core.OrderAggregate;
using MediatR;
using Npgsql;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetActiveOrders;

public class Handler: IRequestHandler<Query, Response>
{
    private readonly string _connectionString;
    
    private Handler()
    {}
    
    public Handler(string connectionString)
    {
        _connectionString = !string.IsNullOrWhiteSpace(connectionString)
            ? connectionString
            : throw new ArgumentNullException(nameof(connectionString));
    }

    
    public async Task<Response> Handle(Query query, CancellationToken cancellationToken)
    {
        await using var connection = new NpgsqlConnection(_connectionString);
        await connection.OpenAsync(cancellationToken);

        var result = await connection.QueryAsync<dynamic>(
            @"SELECT o.id, o.location_x, o.location_y FROM public.orders as o WHERE o.status!=@status", new {status = OrderStatus.Completed.Value});

        if (result.AsList().Count == 0)
            return null;
        
        return new Response(Map(result).ToList());
    }

    private IEnumerable<Order> Map(dynamic result)
    {
        foreach (var item in result)
        {
            Order order = new Order()
                {
                    Id = item.id,
                    Location = new Location()
                        {
                            X = item.location_x,
                            Y = item.location_y
                        }
                };
            yield return order;
        }
    }
}