using Dapper;
using DeliveryApp.Core.CourierAggregate;
using MediatR;
using Npgsql;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetAllCouriers;

public class Handler : IRequestHandler<Query, Response>
{
    private readonly string _connectionString;

    private Handler()
    {
    }

    public Handler(string connectionString)
    {
        _connectionString = !string.IsNullOrWhiteSpace(connectionString)
            ? connectionString
            : throw new ArgumentNullException(nameof(connectionString));
    }

    public async Task<Response> Handle(Query query, CancellationToken cancellationToken)
    {
        await using var connection = new NpgsqlConnection(_connectionString);
        await connection.OpenAsync(cancellationToken);

        var result = await connection.QueryAsync<dynamic>(
            @"SELECT c.id, c.name, c.location_x, c.location_y FROM public.couriers as c WHERE c.status=@statusReady or c.status=@statusBusy", 
            new {statusReady = CourierStatus.Ready.Value, statusBusy = CourierStatus.Busy.Value}
        );

        if (result.AsList().Count == 0)
            return null;

        return new Response(Map(result).ToList());
    }

    private IEnumerable<Courier> Map(dynamic result)
    {
        foreach (var item in result)
        {
            var courier = new Courier()
                {
                    Id = item.id,
                    Name = item.name,
                    Location = new Location()
                        {
                            X = item.location_x,
                            Y = item.location_y
                        }
                };
            yield return courier;
        }
    }
}

