using DeliveryApp.Core.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Ports;
using MediatR;

namespace DeliveryApp.Core.Application.DomainEventHandlers;

public class OrderStatusChangedDomainEventEventHandler : INotificationHandler<OrderStatusChangedDomainEvent>
{
    readonly IBusProducer _busProducer;

    public OrderStatusChangedDomainEventEventHandler(IBusProducer busProducer)
    {
        _busProducer = busProducer;
    }

    public async Task Handle(OrderStatusChangedDomainEvent notification, CancellationToken cancellationToken)
    {
        await _busProducer.Publish(notification, cancellationToken);
    }
}