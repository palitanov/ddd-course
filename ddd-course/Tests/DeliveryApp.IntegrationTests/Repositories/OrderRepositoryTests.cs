using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Core.SharedKernel;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories;

public class OrderRepositoryShould : IAsyncLifetime
{
    private ApplicationDbContext _context;
    
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();
    
    private readonly IMediator _mediator;

    public OrderRepositoryShould()
    {
        _mediator = Substitute.For<IMediator>();
    }

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();
        
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _postgreSqlContainer.GetConnectionString(),
            npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
        _context = new ApplicationDbContext(contextOptions);
        await _context.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void CanAddOrder()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var orderId = Guid.NewGuid();
        var order = Order.Create(orderId, Location.CreateDefault(), Weight.Create(1).Value).Value;
        
        //Act
        var orderRepository = new OrderRepository(_context);
        orderRepository.Add(order);
        await unitOfWork.SaveEntitiesAsync();

        //Assert
        var orderFromDb = await orderRepository.GetAsync(orderId);
        order.Should().BeEquivalentTo(orderFromDb);
    }
    
    [Fact]
    public async void CanUpdateOrder()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var orderId = Guid.NewGuid();
        var order = Order.Create(orderId, Location.CreateDefault(), Weight.Create(1).Value).Value;
        var courier = Courier.Create("Василий", Transport.Scooter).Value;
        courier.StartWork();
        var orderRepository = new OrderRepository(_context);
        orderRepository.Add(order);
        await unitOfWork.SaveEntitiesAsync();
        
        //Act
        var assignResult = order.Assign(courier);
        assignResult.IsSuccess.Should().BeTrue();
        orderRepository.Update(order);
        await unitOfWork.SaveEntitiesAsync();
        
        //Assert
        var orderFromDb = await orderRepository.GetAsync(orderId);
        order.Should().BeEquivalentTo(orderFromDb);
        order.CourierId.Should().Be(courier.Id);
    }
    
    [Fact]
    public async void CanGetById()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var orderId = Guid.NewGuid();
        var order = Order.Create(orderId, Location.CreateDefault(), Weight.Create(1).Value).Value;
        
        //Act
        var orderRepository = new OrderRepository(_context);
        orderRepository.Add(order);
        await unitOfWork.SaveEntitiesAsync();
        
        //Assert
        var orderFromDb = await orderRepository.GetAsync(orderId);
        order.Should().BeEquivalentTo(orderFromDb);
    }
}