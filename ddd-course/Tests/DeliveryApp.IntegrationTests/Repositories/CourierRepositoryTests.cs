using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Core.SharedKernel;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories;

public class CourierRepositoryShould : IAsyncLifetime
{
    private ApplicationDbContext _context;
    private string _connectionString;
    
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();
    
    private readonly IMediator _mediator;

    public CourierRepositoryShould()
    {
        _mediator = Substitute.For<IMediator>();
    }

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();

        _connectionString = _postgreSqlContainer.GetConnectionString();
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _connectionString,
            npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
        _context = new ApplicationDbContext(contextOptions);
        await _context.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void CanAddCourier()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var courier = Courier.Create("Василий", Transport.Scooter).Value;
        var conn = new NpgsqlConnection(_connectionString);
        var command = new NpgsqlCommand("SELECT count(*) FROM couriers where Id = @id", conn);
        command.Parameters.AddWithValue("id", courier.Id);
        conn.Open();
        
        //Act
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        await unitOfWork.SaveEntitiesAsync();

        //Assert
        var count = (Int64)command.ExecuteScalar();
        count.Should().Be(1);
    }
    
    [Fact]
    public async void CanUpdateCourier()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var courier = Courier.Create("Василий", Transport.Scooter).Value;
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        await unitOfWork.SaveEntitiesAsync();
        
        //Act
        courier.StartWork();
        courierRepository.Update(courier);
        await unitOfWork.SaveEntitiesAsync();
        
        //Assert
        var courierFromDb = await courierRepository.GetAsync(courier.Id);
        courier.Should().BeEquivalentTo(courierFromDb);
        courier.Status.Should().Be(CourierStatus.Ready);
    }
    
    [Fact]
    public async void CanGetById()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var courier = Courier.Create("Василий", Transport.Scooter).Value;
        
        //Act
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        await unitOfWork.SaveEntitiesAsync();

        //Assert
        var courierFromDb = await courierRepository.GetAsync(courier.Id);
        courier.Should().BeEquivalentTo(courierFromDb);
    }
    
    [Fact]
    public async void CanGetAllReady()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var courier = Courier.Create("ФИО", Transport.Pedestrian).Value;
        var courier1 = Courier.Create("ФИО 1", Transport.Bicycle).Value;
        var courier2 = Courier.Create("ФИО 2", Transport.Scooter).Value;
        var courier3 = Courier.Create("ФИО 3", Transport.Car).Value;
        courier2.StartWork(); 
        courier3.StartWork();
        
        var courierRepository = new CourierRepository(_context);
        var c = courierRepository.Add(courier);
        var c1 =courierRepository.Add(courier1);
        var c2 =courierRepository.Add(courier2);
        var c3 =courierRepository.Add(courier3);
        await unitOfWork.SaveEntitiesAsync();
        
        //Act
        var allReady = courierRepository.GetAllReady().ToList();
        
        //Assert
        allReady.Should().NotBeNull();
        allReady.All(x => x.Status == CourierStatus.Ready).Should().BeTrue();
        allReady.Should().Contain(courier2);
        allReady.Should().Contain(courier3);
    }
    
    [Fact]
    public async void CanGetAllBusy()
    {
        //Arrange
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var order = Order.Create(Guid.NewGuid(), Location.CreateDefault(), Weight.Create(1).Value).Value;
        var courier = Courier.Create("ФИО", Transport.Pedestrian).Value;
        var courier1 = Courier.Create("ФИО 1", Transport.Bicycle).Value;
        var courier2 = Courier.Create("ФИО 2", Transport.Scooter).Value;
        var courier3 = Courier.Create("ФИО 3", Transport.Car).Value;
        courier2.StartWork(); 
        courier3.StartWork();
        order.Assign(courier2);
        order.Assign(courier3);
        
        var courierRepository = new CourierRepository(_context);
        var c = courierRepository.Add(courier);
        var c1 =courierRepository.Add(courier1);
        var c2 =courierRepository.Add(courier2);
        var c3 =courierRepository.Add(courier3);
        await unitOfWork.SaveEntitiesAsync();
        
        //Act
        var allBusy = courierRepository.GetAllBusy().ToList();
        
        //Assert
        allBusy.Should().NotBeNull();
        allBusy.All(x => x.Status == CourierStatus.Busy).Should().BeTrue();
        allBusy.Should().Contain(courier2);
        allBusy.Should().Contain(courier3);
    }
}