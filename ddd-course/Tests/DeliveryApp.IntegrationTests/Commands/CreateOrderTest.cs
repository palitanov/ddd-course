using DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;
using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Core.Ports;
using DeliveryApp.Core.SharedKernel;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;
using Handler = DeliveryApp.Core.Application.UseCases.Commands.CreateOrder.Handler;

namespace DeliveryApp.IntegrationTests.Commands;

public class CreateOrderHandlerShould: IAsyncLifetime
{
    private ApplicationDbContext _context;
    private string _connectionString;
    
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();
    
    private readonly IMediator _mediator;

    public CreateOrderHandlerShould()
    {
        _mediator = Substitute.For<IMediator>();
    }

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();

        _connectionString = _postgreSqlContainer.GetConnectionString();
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _connectionString,
            npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
        _context = new ApplicationDbContext(contextOptions);
        await _context.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void ShouldHandle()
    {
        //Arrange
        var orderId = Guid.NewGuid();
        var orderRepository = new OrderRepository(_context);
        var unitOfWork = new UnitOfWork(_context, _mediator);
        string orderAddress = "Серверная";
        var orderLocation = Location.Create(6,6).Value;
        var client = Substitute.For<IGeoClient>();
        client.GetGeolocationAsync(orderAddress, CancellationToken.None).Returns(orderLocation);
        
        var command = new Command(orderId, orderAddress, 1);
        var handler = new Handler(orderRepository, unitOfWork, client);
        
        //Act
        var result = await handler.Handle(command, CancellationToken.None);

        //Assert
        var order = await orderRepository.GetAsync(orderId);
        result.Should().Be(true);
        order.Location.Should().Be(orderLocation);
        order.Status.Should().Be(OrderStatus.Created);
    }
}