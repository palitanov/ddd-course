using DeliveryApp.Core.Application.UseCases.Commands.AssignOrderToCourier;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.DomainServices;
using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;
using Handler = DeliveryApp.Core.Application.UseCases.Commands.AssignOrderToCourier.Handler;

namespace DeliveryApp.IntegrationTests.Commands;

public class AssignOrderToCourierHandlerShould: IAsyncLifetime
{
    private ApplicationDbContext _context;
    private string _connectionString;
    
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();
    
    private readonly IMediator _mediator;

    public AssignOrderToCourierHandlerShould()
    {
        _mediator = Substitute.For<IMediator>();
    }

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();

        _connectionString = _postgreSqlContainer.GetConnectionString();
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _connectionString,
            npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
        _context = new ApplicationDbContext(contextOptions);
        await _context.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void ShouldHandle()
    {
        //Arrange
        
        string sql = $"""
                      INSERT INTO public.orders 
                          (id, courier_id, location_x, location_y, weight, status) 
                        VALUES('9e681189-4c0f-4e67-92a6-1913ed68497d', '00000000-0000-0000-0000-000000000000', 5, 5, 2, 'created');

                        INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('bf79a004-56d7-4e5f-a21c-0a9e5e08d10d', 'Петя', 1, 1, 3, 'ready');
                          
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('a9f7e4aa-becc-40ff-b691-f063c5d04015', 'Оля', 2, 3,2, 'ready');
                          
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('db18375d-59a7-49d1-bd96-a1738adcee93', 'Ваня', 3, 4,5, 'ready');
                      """;
        var executeSqlRaw = _context.Database.ExecuteSqlRaw(sql);
        
        var orderId = Guid.Parse("9e681189-4c0f-4e67-92a6-1913ed68497d");
        var courierId = Guid.Parse("db18375d-59a7-49d1-bd96-a1738adcee93");
        var courierRepository = new CourierRepository(_context);
        var orderRepository = new OrderRepository(_context);
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var dispatchService = new DispatchService();
        
        var command = new Command();
        var handler = new Handler(orderRepository, courierRepository, unitOfWork, dispatchService);
        
        //Act
        var result = await handler.Handle(command, CancellationToken.None);

        //Assert
        var courier = await courierRepository.GetAsync(courierId);
        var order = await orderRepository.GetAsync(orderId);
        result.IsSuccess.Should().Be(true);
        order.CourierId.Should().Be(courierId);
        order.Status.Should().Be(OrderStatus.Assigned);
        courier.Status.Should().Be(CourierStatus.Busy);
    }
    
    [Fact]
    public async void ShouldReturnErrorThereAreNoCouriersWithSuitableTransportOnHandle()
    {
        //Arrange
        string sql = $"""
                      INSERT INTO public.orders 
                          (id, courier_id, location_x, location_y, weight, status) 
                        VALUES('9e681189-4c0f-4e67-92a6-1913ed68497d', '00000000-0000-0000-0000-000000000000', 5, 5, 10, 'created');

                        INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('bf79a004-56d7-4e5f-a21c-0a9e5e08d10d', 'Петя', 1, 1, 3, 'ready');
                          
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('a9f7e4aa-becc-40ff-b691-f063c5d04015', 'Оля', 2, 3,2, 'ready');
                          
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('db18375d-59a7-49d1-bd96-a1738adcee93', 'Ваня', 3, 4,5, 'ready');
                      """;
        var executeSqlRaw = _context.Database.ExecuteSqlRaw(sql);
        
        var courierRepository = new CourierRepository(_context);
        var orderRepository = new OrderRepository(_context);
        var unitOfWork = new UnitOfWork(_context, _mediator);
        var dispatchService = new DispatchService();
        
        var command = new Command();
        var handler = new Handler(orderRepository, courierRepository, unitOfWork, dispatchService);
        
        //Act
        var result = await handler.Handle(command, CancellationToken.None);

        //Assert
        result.Error.Should().Be(DispatchService.Errors.ThereAreNoCouriersWithSuitableTransport());
    }
    
    
}