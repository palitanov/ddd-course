using DeliveryApp.Core.Application.UseCases.Commands.CourierStartWork;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;
using Handler = DeliveryApp.Core.Application.UseCases.Commands.CourierStartWork.Handler;

namespace DeliveryApp.IntegrationTests.Commands;

public class CourierStartWorkHandlerShould: IAsyncLifetime
{
    private ApplicationDbContext _context;
    private string _connectionString;
    
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();
    
    private readonly IMediator _mediator;

    public CourierStartWorkHandlerShould()
    {
        _mediator = Substitute.For<IMediator>();
    }

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();

        _connectionString = _postgreSqlContainer.GetConnectionString();
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _connectionString,
            npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
        _context = new ApplicationDbContext(contextOptions);
        await _context.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void ShouldHandle()
    {
        //Arrange
        var courier = Courier.Create("Петя", Transport.Pedestrian).Value;
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        var unitOfWork = new UnitOfWork(_context, _mediator);
        await unitOfWork.SaveEntitiesAsync(CancellationToken.None);
        
        var command = new Command(courier.Id);
        var handler = new Handler(courierRepository, unitOfWork);
        
        //Act
        var result = await handler.Handle(command, CancellationToken.None);

        //Assert
        var courierFromDb = await courierRepository.GetAsync(courier.Id);
        result.Should().Be(true);
        courierFromDb.Status.Should().Be(CourierStatus.Ready);
    }
}