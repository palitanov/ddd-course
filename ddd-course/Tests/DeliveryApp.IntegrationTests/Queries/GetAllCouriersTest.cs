using DeliveryApp.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Testcontainers.PostgreSql;
using Xunit;
using DeliveryApp.Core.Application.UseCases.Queries.GetAllCouriers;
using FluentAssertions;

namespace DeliveryApp.IntegrationTests.Queries;

public class GetAllCouriersHandlerShould: IAsyncLifetime
{
    private ApplicationDbContext _context;
    private string _connectionString;
    
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();
    

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();

        _connectionString = _postgreSqlContainer.GetConnectionString();
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
            _connectionString,
            npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
        _context = new ApplicationDbContext(contextOptions);
        await _context.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void ReturnResponseOnHandle()
    {
        //Arrange
        string sql = $"""
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('bf79a004-56d7-4e5f-a21c-0a9e5e08d10d', 'Петя', 1, 1, 3, 'ready');
                          
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('a9f7e4aa-becc-40ff-b691-f063c5d04015', 'Оля', 1, 3,2, 'busy');
                          
                      INSERT INTO public.couriers(
                          id, name, transport_id, location_x, location_y, status)
                          VALUES ('db18375d-59a7-49d1-bd96-a1738adcee93', 'Ваня', 2, 4,5, 'notavailable');
                      """;
        var executeSqlRaw = _context.Database.ExecuteSqlRaw(sql);
        var query = new Query();
        var handler = new Handler(_connectionString);
        
        //Act
        var response = await handler.Handle(query, CancellationToken.None);

        //Assert
        response.Couriers.Count.Should().Be(2);
        response.Couriers.Should().Contain(x => x.Id == Guid.Parse("bf79a004-56d7-4e5f-a21c-0a9e5e08d10d"));
        response.Couriers.Should().Contain(x => x.Id == Guid.Parse("a9f7e4aa-becc-40ff-b691-f063c5d04015"));
        response.Couriers.Should().NotContain(x => x.Id == Guid.Parse("db18375d-59a7-49d1-bd96-a1738adcee93"));
    }
}