using DeliveryApp.Core.SharedKernel;
using FluentAssertions;
using Xunit;

namespace DeliveryApp.UnitTests.SharedKernel;

public class LocationShould
{
    [Theory]
    [InlineData(1, 1)]
    [InlineData(1,10)]
    [InlineData(10, 1)]
    [InlineData(10, 10)]
    [InlineData(5, 5)]
    public void BeCorrectWhenParamsIsCorrectOnCreated(int x, int y)
    {
        //Arrange
        
        //Act
        var location = Location.Create(x,y);

        //Assert
        location.IsSuccess.Should().BeTrue();
        location.Value.X.Should().Be(x);
        location.Value.Y.Should().Be(y);
    }

    [Theory]
    [InlineData(0, 1)]
    [InlineData(1,0)]
    [InlineData(11, 1)]
    [InlineData(1, 11)]
    [InlineData(-1, -1)]
    public void ReturnErrorWhenParamsIsInCorrectOnCreated(int x, int y)
    {
        //Arrange

        //Act
        var location = Location.Create(x, y);

        //Assert
        location.IsSuccess.Should().BeFalse();
        location.Error.Should().NotBeNull();
    }

    [Fact]
    public void BeEqualWhenAllPropertiesIsEqual()
    {
        //Arrange
        var first = Location.Create(10, 10).Value;
        var second = Location.Create(10, 10).Value;

        //Act
        var result = first == second;

        //Assert
        result.Should().BeTrue();
    }

    [Fact]
    public void BeNotEqualWhenAllPropertiesIsEqual()
    {
        //Arrange
        var first = Location.Create(10, 10).Value;
        var second = Location.Create(5, 5).Value;

        //Act
        var result = first == second;

        //Assert
        result.Should().BeFalse();
    }

    [Fact]
    public void CanCalculateDistance()
    {
        //Arrange
        var first = Location.Create(1, 1).Value;
        var second = Location.Create(6, 7).Value;
        int expectedDistance = 11; // |1 - 6| + |1 - 7| = 5 + 6 = 11

        //Act
        var result = first.DistanceTo(second);

        //Assert
        result.Value.Should().Be(expectedDistance);
    }
}