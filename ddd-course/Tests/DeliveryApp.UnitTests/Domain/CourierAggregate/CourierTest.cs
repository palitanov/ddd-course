using System;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.SharedKernel;
using FluentAssertions;
using Xunit;

public class CourierShould
{
    
    private const string CourierName = "Василий";
    
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var transport = Transport.Pedestrian;

        //Act
        var courier = Courier.Create(CourierName, transport);

        //Assert
        courier.IsSuccess.Should().BeTrue();
        courier.Value.Name.Should().Be(CourierName);
        courier.Value.Transport.Should().Be(transport);
        courier.Value.Location.Should().Be(Location.Default);
        courier.Value.Status.Should().Be(CourierStatus.NotAvailable);
    }

    [Fact]
    public void CanStartWork()
    {
        //Arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian).Value;
        
        //Act
        var result = courier.StartWork();
        
        //Assert
        result.IsSuccess.Should().BeTrue();
        courier.Status.Should().Be(CourierStatus.Ready);
    }
    
    [Fact]
    public void CanStopWork()
    {
        //Arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian).Value;
        
        //Act
        var result = courier.StopWork();
        
        //Assert
        result.IsSuccess.Should().BeTrue();
        courier.Status.Should().Be(CourierStatus.NotAvailable);
    }
    
    [Fact]
    public void CanInWorkWhenCourierIsReady()
    {
        //Arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian).Value;
        courier.StartWork();
        
        //Act
        var result = courier.InWork();
        
        //Assert
        result.IsSuccess.Should().BeTrue();
        courier.Status.Should().Be(CourierStatus.Busy);
    }
    
    [Fact]
    public void ReturnErrorWhenCourierStatusIsNotReadyOnInWork()
    {
        //Arrange
        var courier = Courier.Create(CourierName, Transport.Pedestrian).Value;
        
        //Act
        var result = courier.InWork();
        
        //Assert
        result.IsSuccess.Should().BeFalse();
        courier.Status.Should().NotBe(CourierStatus.Busy);
    }
    
    [Fact]
    public void CanMove()
    {
        //Arrange
        var courier = Courier.Create(CourierName, Transport.Bicycle).Value;
        var orderLocation = Location.Create(5,5).Value;
        var expectedLocation = Location.Create(3,1).Value;
        
        //Act
        var result = courier.Move(orderLocation);
            
        //Assert
        result.IsSuccess.Should().BeTrue();
        courier.Location.Should().Be(expectedLocation);
    }
    
    
    [Theory]
    [InlineData(1, 5, 5)]
    [InlineData(1, 10, 10)]
    [InlineData(4, 10, 10)]
    [InlineData(2, 5, 5)]
    [InlineData(3, 5, 5)]
    public void CanCalculateTimeToPoint(int transportId, int x, int y)
    {
        //Arrange
        var transport = Transport.FromId(transportId).Value;
        var courier = Courier.Create(CourierName, transport).Value;
        var location = Location.Create(x, y).Value;
        var distance = Math.Abs(location.X - courier.Location.X) + Math.Abs(location.Y - courier.Location.Y);
        var time = (double)distance / transport.Speed;
        
        //Act
        var result = courier.CalculateTimeToPoint(location);
        
        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Should().Be(time);
    }
    
    [Theory]
    [InlineData(1, 5, 5, 8)]
    [InlineData(2, 5, 5, 4)]
    [InlineData(3, 5, 5, 3)]
    [InlineData(4, 5, 5, 2)]
    [InlineData(1, 10, 10, 18)]
    [InlineData(2, 10, 10, 9)]
    [InlineData(3, 10, 10, 6)]
    [InlineData(4, 10, 10, 5)]
    public void CanReachOrderLocation(int transportId, int x, int y, int iterationCount)
    {
        //Arrange
        var courier = Courier.Create(CourierName, Transport.FromId(transportId).Value).Value;
        var orderLocation = Location.Create(x,y).Value;
        
        //Act
        for (int i = 0; i < iterationCount; i++)
        {
            var result = courier.Move(orderLocation);
            result.IsSuccess.Should().BeTrue();
        }
        
        //Assert
        courier.Location.Should().Be(orderLocation);
    }
}