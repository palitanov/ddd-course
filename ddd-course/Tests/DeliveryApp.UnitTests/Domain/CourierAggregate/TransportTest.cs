using System.Collections.Generic;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.SharedKernel;
using FluentAssertions;
using Xunit;

public class TransportShould
{
    public static IEnumerable<object[]> GetTransports()
    {
        yield return [1,"pedestrian", 1, 1];
        yield return [2,"bicycle", 2, 4];
        yield return [3,"scooter", 3, 6];
        yield return [4,"car", 4, 8];
    }
    
    [Fact]
    public void BeCorrectWhenParamsIsCorrectOnCreated()
    {
        //Arrange

        //Act
        var transport = Transport.Pedestrian;

        //Assert
        transport.Name.Should().Be("pedestrian");
        transport.Capacity.Should().Be(Weight.Create(1).Value);
        transport.Speed.Should().Be(1);
    }
    
    [Theory]
    [MemberData(nameof(GetTransports))]
    public void BeCorrectWhenParamsIsCorrectOnCreatedFromName(int id, string name, int speed, int weight)
    {
        //Arrange

        //Act
        var result = Transport.FromName(name);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Id.Should().Be(id);
        result.Value.Name.Should().Be(name);
        result.Value.Capacity.Should().Be(Weight.Create(weight).Value);
        result.Value.Speed.Should().Be(speed);
    }
    
    [Theory]
    [MemberData(nameof(GetTransports))]
    public void BeCorrectWhenParamsIsCorrectOnCreatedFromId(int id, string name, int speed, int weight)
    {
        //Arrange

        //Act
        var result = Transport.FromId(id);

        //Assert
        result.IsSuccess.Should().BeTrue();
        result.Value.Id.Should().Be(id);
        result.Value.Name.Should().Be(name);
        result.Value.Capacity.Should().Be(Weight.Create(weight).Value);
        result.Value.Speed.Should().Be(speed);
    }

    [Fact]
    public void ReturnErrorWhenTransportNotFoundByName()
    {
        //Arrange

        //Act
        var result = Transport.FromName("Лодка");
        
        //Assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().NotBeNull();
    }
    
    [Fact]
    public void ReturnErrorWhenTransportNotFoundById()
    {
        //Arrange

        //Act
        var result = Transport.FromId(0);
        
        //Assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().NotBeNull();
    }
    
    [Fact]
    public void ReturnListOfTransport()
    {
        //Arrange

        //Act
        var allTransports = Transport.List();

        //Assert
        allTransports.Should().NotBeEmpty();
    }

    [Fact]
    public void CanCarryWeight()
    {
        //Arrange
        var transport = Transport.Car;
        var weight = Weight.Create(5).Value;

        //Act
        var result = transport.CanCarryWeight(weight);
        
        //Assert
        result.Value.Should().BeTrue();
    }
    
    [Fact]
    public void CanNotCarryWeight()
    {
        //Arrange
        var transport = Transport.Pedestrian;
        var weight = Weight.Create(5).Value;

        //Act
        var result = transport.CanCarryWeight(weight);
        
        //Assert
        result.Value.Should().BeFalse();
    }
    
}