using System;
using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Core.SharedKernel;
using FluentAssertions;
using Primitives;
using Xunit;

public class OrderShould
{
    [Fact]
    public void BeCorrectWhenParamsIsCorrect()
    {
        //Arrange
        var basketId = Guid.NewGuid();
        var location = Location.CreateDefault();
        var weight = Weight.Create(1).Value;

        //Act
        var order = Order.Create(basketId, location, weight);

        //Assert
        order.IsSuccess.Should().BeTrue();
        order.Value.Id.Should().Be(basketId);
        order.Value.Location.Should().Be(location);
        order.Value.Weight.Should().Be(weight);
        order.Value.Status.Should().Be(OrderStatus.Created);
    }

    [Fact]
    public void ReturnValueIsRequiredErrorWhenIdIsEmpty()
    {
        //Arrange
        var id = Guid.Empty;

        //Act
        var result = Order.Create(id, null, null);

        //Assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().Be(GeneralErrors.ValueIsRequired(nameof(id)));
    }

    [Fact]
    public void CanAssignCourier()
    {
        //Arrange
        var id = Guid.NewGuid();
        var order = Order.Create(id, Location.CreateDefault(), Weight.Create(1).Value).Value;
        var courier = Courier.Create("Name", Transport.Pedestrian).Value;
        courier.StartWork();
        
        //Act
        var result = order.Assign(courier);
        
        //Assert
        result.IsSuccess.Should().BeTrue();
        order.CourierId.Should().Be(courier.Id);
        order.Status.Should().Be(OrderStatus.Assigned);
    }
    
    [Fact]
    public void ReturnErrorIfAssignNotAvailableCourier()
    {
        //Arrange
        var id = Guid.NewGuid();
        var order = Order.Create(id, Location.CreateDefault(), Weight.Create(1).Value).Value;
        var courier = Courier.Create("Name", Transport.Pedestrian).Value;
        
        //Act
        var result = order.Assign(courier);
        
        //Assert
        result.IsSuccess.Should().BeFalse();
        order.Status.Should().NotBe(OrderStatus.Assigned);
    }
    
    [Fact]
    public void CanCompleteAssignedOrder()
    {
        //Arrange
        var id = Guid.NewGuid();
        var order = Order.Create(id, Location.CreateDefault(), Weight.Create(1).Value).Value;
        var courier = Courier.Create("Name", Transport.Pedestrian).Value;
        courier.StartWork();
        order.Assign(courier);
        
        //Act
        var result = order.Complete();
        
        //Assert
        result.IsSuccess.Should().BeTrue();
        order.Status.Should().Be(OrderStatus.Completed);
    }
    
    [Fact]
    public void ReturnErrorIfTryCompleteNotAssignedOrder()
    {
        //Arrange
        var id = Guid.NewGuid();
        var order = Order.Create(id, Location.CreateDefault(), Weight.Create(1).Value).Value;
        var courier = Courier.Create("Name", Transport.Pedestrian).Value;
        
        //Act
        var result = order.Complete();
        
        //Assert
        result.IsSuccess.Should().BeFalse();
        result.Error.Should().Be(Order.Errors.OrderStatusShouldBeAssigned());
    }
}