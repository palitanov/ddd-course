using Api.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryApp.Api.Adapters.Http;

public class DeliveryController : DefaultApiController
{
    private readonly IMediator _mediator;

    public DeliveryController(IMediator mediator)
    { 
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }
    
    public override async Task<IActionResult> GetActiveOrders()
    {
        var query = new Core.Application.UseCases.Queries.GetActiveOrders.Query();
        
        var response = await _mediator.Send(query);
        return Ok(response);
    }

    public override async Task<IActionResult> GetAllCouriers()
    {
        var query = new Core.Application.UseCases.Queries.GetAllCouriers.Query();
        
        var response = await _mediator.Send(query);
        return Ok(response);
    }

    public override async Task<IActionResult> StartWork(Guid courierId)
    {
        var command = new Core.Application.UseCases.Commands.CourierStartWork.Command(courierId);
        
        var response = await _mediator.Send(command);
        if (response) return Ok();
        return Conflict();
    }

    public override async Task<IActionResult> StopWork(Guid courierId)
    {
        var command = new Core.Application.UseCases.Commands.CourierStopWork.Command(courierId);
        
        var response = await _mediator.Send(command);
        if (response) return Ok();
        return Conflict();
    }
}