using System.Reflection;
using Api.Filters;
using Api.Formatters;
using Api.OpenApi;
using CSharpFunctionalExtensions;
using DeliveryApp.Api.Adapters.BackgroundJobs;
using DeliveryApp.Api.Adapters.Kafka.BasketConfirmed;
using DeliveryApp.Core.Application.DomainEventHandlers;
using DeliveryApp.Core.DomainServices;
using DeliveryApp.Core.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Ports;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Microsoft.OpenApi.Models;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Grpc.GeoService;
using DeliveryApp.Infrastructure.Adapters.Kafka.OrderStatusChanged;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using DeliveryApp.Infrastructure.BackgroundJobs;
using MediatR;
using Primitives;
using Quartz;


namespace DeliveryApp.Api
{
    public class Startup
    {
        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables();
            var configuration = builder.Build();
            Configuration = configuration;
        }

        /// <summary>
        /// Конфигурация
        /// </summary>
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Health Checks
            services.AddHealthChecks();

            // Cors
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    policy =>
                    {
                        policy.AllowAnyOrigin(); // Не делайте так в проде!
                    });
            });
            
            // Configuration
            services.Configure<Settings>(options => Configuration.Bind(options));
            var connectionString = Configuration["CONNECTION_STRING"];
            var geoServiceGrpcHost = Configuration["GEO_SERVICE_GRPC_HOST"];
            var messageBrokerHost = Configuration["MESSAGE_BROKER_HOST"];
            
            // БД 
            services.AddDbContext<ApplicationDbContext>(options =>
                {
                    options.UseNpgsql(connectionString,
                        npgsqlOptionsAction: sqlOptions =>
                        {
                            sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                        });
                    options.EnableSensitiveDataLogging();
                }
            );
            services.AddTransient<IUnitOfWork, UnitOfWorkV2>();
            
            // Ports & Adapters
            services.AddTransient<ICourierRepository, CourierRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IGeoClient>(x => new Client(geoServiceGrpcHost));
            services.AddTransient<IBusProducer>(x=> new Producer(messageBrokerHost));
            
            //Domain services
            services.AddTransient<IDispatchService, DispatchService>();
            
            // MediatR 
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<Startup>());

            // Commands
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Commands.CreateOrder.Command, bool>,
                Core.Application.UseCases.Commands.CreateOrder.Handler>();
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Commands.MoveCouriers.Command, bool>,
                Core.Application.UseCases.Commands.MoveCouriers.Handler>();
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Commands.AssignOrderToCourier.Command, Result<object, Error>>,
                Core.Application.UseCases.Commands.AssignOrderToCourier.Handler>();
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Commands.CourierStartWork.Command, bool>,
                Core.Application.UseCases.Commands.CourierStartWork.Handler>();
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Commands.CourierStopWork.Command, bool>,
                Core.Application.UseCases.Commands.CourierStopWork.Handler>();

            // Queries
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Queries.GetActiveOrders.Query,
                Core.Application.UseCases.Queries.GetActiveOrders.Response>>(x
                => new Core.Application.UseCases.Queries.GetActiveOrders.Handler(connectionString));
            services.AddTransient<IRequestHandler<Core.Application.UseCases.Queries.GetAllCouriers.Query,
                Core.Application.UseCases.Queries.GetAllCouriers.Response>>(x
                => new Core.Application.UseCases.Queries.GetAllCouriers.Handler(connectionString));
            
            // HTTP Handlers
            services.AddControllers(options =>
                    {
                        options.InputFormatters.Insert(0, new InputFormatterStream());
                    })
                .AddNewtonsoftJson(options =>
                    {
                        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        options.SerializerSettings.Converters.Add(new StringEnumConverter
                            {
                                NamingStrategy = new CamelCaseNamingStrategy()
                            });
                    });

            // Swagger
            services.AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("1.0.0", new OpenApiInfo
                        {
                            Title = "Delivery Service",
                            Description = "Отвечает за учет курьеров, деспетчеризацию доставкуов, доставку",
                        });
                    options.CustomSchemaIds(type => type.FriendlyId(true));
                    options.IncludeXmlComments($"{AppContext.BaseDirectory}{Path.DirectorySeparatorChar}{Assembly.GetEntryAssembly().GetName().Name}.xml");
                    options.DocumentFilter<BasePathFilter>("");
                    options.OperationFilter<GeneratePathParamsValidationFilter>();
                });
            services.AddSwaggerGenNewtonsoftSupport();

            services.AddTransient<INotificationHandler<OrderStatusChangedDomainEvent>, OrderStatusChangedDomainEventEventHandler>();
            
            // CRON Jobs
            services.AddQuartz(configure =>
                {
                    var assignOrdersJobKey = new JobKey(nameof(AssignOrdersJob));
                    var moveCouriersJobKey = new JobKey(nameof(MoveCouriersJob));
                    configure
                        .AddJob<AssignOrdersJob>(assignOrdersJobKey)
                        .AddTrigger(
                            trigger => trigger.ForJob(assignOrdersJobKey)
                                .WithSimpleSchedule(
                                    schedule => schedule.WithIntervalInSeconds(5)
                                        .RepeatForever()))
                        .AddJob<MoveCouriersJob>(moveCouriersJobKey)
                        .AddTrigger(
                            trigger => trigger.ForJob(moveCouriersJobKey)
                                .WithSimpleSchedule(
                                    schedule => schedule.WithIntervalInSeconds(3)
                                        .RepeatForever()));
                    configure.UseMicrosoftDependencyInjectionJobFactory();
                });
            
            services.AddQuartz(configure =>
                {
                    var processOutboxMessagesJobKey = new JobKey(nameof(ProcessOutboxMessagesJob));
                    configure
                        .AddJob<ProcessOutboxMessagesJob>(processOutboxMessagesJobKey)
                        .AddTrigger(
                            trigger => trigger.ForJob(processOutboxMessagesJobKey)
                                .WithSimpleSchedule(
                                    schedule => schedule.WithIntervalInSeconds(5)
                                        .RepeatForever()));
                    configure.UseMicrosoftDependencyInjectionJobFactory();
                });
            services.AddQuartzHostedService();
            
            services.AddGrpcClient<Client>(options => 
                { 
                    options.Address = new Uri (geoServiceGrpcHost); 
                });
            
            // Message Broker
            var sp = services.BuildServiceProvider();
            var mediator = sp.GetService<IMediator>();
            services.AddHostedService(x => new ConsumerService(mediator,messageBrokerHost));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseRouting();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseSwagger(c =>
                    {
                        c.RouteTemplate = "openapi/{documentName}/openapi.json";
                    })
                .UseSwaggerUI(options =>
                    {
                        options.RoutePrefix = "openapi";
                        options.SwaggerEndpoint("/openapi/1.0.0/openapi.json", "Swagger Delivery Service");
                        options.RoutePrefix = string.Empty;
                        options.SwaggerEndpoint("/openapi-original.json", "Swagger Delivery Service");
                    });

            app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
            
            app.UseHealthChecks("/health");
            app.UseRouting();
        }
    }
}