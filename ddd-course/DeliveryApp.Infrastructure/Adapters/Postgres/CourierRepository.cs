using DeliveryApp.Core.CourierAggregate;
using DeliveryApp.Core.Ports;
using Microsoft.EntityFrameworkCore;
using Primitives;

namespace DeliveryApp.Infrastructure.Adapters.Postgres;

public class CourierRepository(ApplicationDbContext dbContext) : ICourierRepository
{
    private readonly ApplicationDbContext _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    
    public Courier Add(Courier courier)
    {
        if (courier.Transport != null)
        {
            _dbContext.Attach(courier.Transport);
        }
        return _dbContext.Couriers.Add(courier).Entity;
    }

    public void Update(Courier courier)
    {
        if (courier.Transport != null)
        {
            _dbContext.Attach(courier.Transport);
        }
        _dbContext.Entry(courier).State = EntityState.Modified;
    }

    public async Task<Courier> GetAsync(Guid courierId)
    {
        var courier = await _dbContext.
            Couriers.
            Include(x => x.Transport).
            FirstOrDefaultAsync(x => x.Id == courierId);
        if (courier == null)
        {
            courier = _dbContext.Couriers.Local.FirstOrDefault(x => x.Id == courierId);
        }
        return courier;
    }

    public IEnumerable<Courier> GetAllReady()
    {
        return _dbContext.Couriers.Include(x=>x.Transport).Where(x => x.Status.Value == CourierStatus.Ready.Value);
    }

    public IEnumerable<Courier> GetAllBusy()
    {
        return _dbContext.Couriers.Include(x=>x.Transport).Where(x => x.Status.Value == CourierStatus.Busy.Value);
    }
}