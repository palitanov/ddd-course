using DeliveryApp.Core.OrderAggregate;
using DeliveryApp.Core.Ports;
using Microsoft.EntityFrameworkCore;
using Primitives;

namespace DeliveryApp.Infrastructure.Adapters.Postgres;

public class OrderRepository (ApplicationDbContext dbContext) : IOrderRepository
{
    private readonly ApplicationDbContext _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
    
    public Order Add(Order order)
    {
        return _dbContext.Orders.Add(order).Entity;
    }

    public void Update(Order order)
    {
        _dbContext.Entry(order).State = EntityState.Modified;
    }

    public async Task<Order> GetAsync(Guid orderId)
    {
        var order = await _dbContext.
            Orders.
            FirstOrDefaultAsync(x => x.Id == orderId);
        if (order == null)
        {
            order = _dbContext.Orders.Local.FirstOrDefault(x => x.Id == orderId);
        }
        return order;
    }

    public IEnumerable<Order> GetAllNotAssigned()
    {
        return _dbContext.Orders.Where(x => x.Status.Value == OrderStatus.Created.Value);
    }

    public IEnumerable<Order> GetAllAssigned()
    {
        return _dbContext.Orders.Where(x => x.Status.Value == OrderStatus.Assigned.Value);
    }
}