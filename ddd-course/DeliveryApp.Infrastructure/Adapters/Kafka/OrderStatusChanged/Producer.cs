using Confluent.Kafka;
using DeliveryApp.Core.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Ports;
using Newtonsoft.Json;
using OrderStatusChanged;


namespace DeliveryApp.Infrastructure.Adapters.Kafka.OrderStatusChanged;

public sealed class Producer: IBusProducer
{
    private readonly ProducerConfig _config;
    private readonly string _topicName = "order.status.changed";

    public Producer(string messageBrokerHost)
    {
        if (string.IsNullOrWhiteSpace(messageBrokerHost)) throw new ArgumentException(nameof(messageBrokerHost));
        _config = new ProducerConfig
            {
                BootstrapServers = messageBrokerHost
            };
    }

    public async Task Publish(OrderStatusChangedDomainEvent notification, CancellationToken cancellationToken)
    {
        var orderStatusChangedIntegrationEvent = new OrderStatusChangedIntegrationEvent()
            {
                OrderId = notification.BasketId.ToString(),
                OrderStatus = GetStatus(notification.OrderStatus)
            };
        
        var message = new Message<string, string>
            {
                Key = notification.EventId.ToString(),
                Value = JsonConvert.SerializeObject(orderStatusChangedIntegrationEvent)
            };
        
        using var producer = new ProducerBuilder<string, string>(_config).Build();
        await producer.ProduceAsync(_topicName, message, cancellationToken);
    }

    private static OrderStatus GetStatus(Core.OrderAggregate.OrderStatus status) =>
        status switch
            {
                _ when status == Core.OrderAggregate.OrderStatus.Created => OrderStatus.Created,
                _ when status == Core.OrderAggregate.OrderStatus.Assigned => OrderStatus.Assigned,
                _ when status == Core.OrderAggregate.OrderStatus.Completed => OrderStatus.Completed,
                _ => OrderStatus.None
            };
}