using CSharpFunctionalExtensions;
using DeliveryApp.Core.Ports;
using GeoApp.Api;
using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.Client.Configuration;
using Primitives;
using Location = DeliveryApp.Core.SharedKernel.Location;

namespace DeliveryApp.Infrastructure.Adapters.Grpc.GeoService;

public class Client : IGeoClient
{
    private readonly MethodConfig _methodConfig;
    private readonly SocketsHttpHandler _socketsHttpHandler;
    private readonly string _url;

    public Client(string url)
    {
        _url = url;

        _socketsHttpHandler = new SocketsHttpHandler
            {
                PooledConnectionIdleTimeout = Timeout.InfiniteTimeSpan,
                KeepAlivePingDelay = TimeSpan.FromSeconds(60),
                KeepAlivePingTimeout = TimeSpan.FromSeconds(30),
                EnableMultipleHttp2Connections = true
            };

        _methodConfig = new MethodConfig
            {
                Names = { MethodName.Default },
                RetryPolicy = new RetryPolicy
                    {
                        MaxAttempts = 5,
                        InitialBackoff = TimeSpan.FromSeconds(1),
                        MaxBackoff = TimeSpan.FromSeconds(5),
                        BackoffMultiplier = 1.5,
                        RetryableStatusCodes = { StatusCode.Unavailable }
                    }
            };
    }

    public async Task<Result<Location, Error>> GetGeolocationAsync(string address, CancellationToken cancellationToken)
    {
        using var channel = GrpcChannel.ForAddress(_url, new GrpcChannelOptions
            {
                HttpHandler = _socketsHttpHandler,
                ServiceConfig = new ServiceConfig { MethodConfigs = { _methodConfig } }
            });

        var client = new Geo.GeoClient(channel);
        try
        {
            var reply = await client.GetGeolocationAsync(new GetGeolocationRequest
                {
                    Address = address
                }, null, DateTime.UtcNow.AddSeconds(5), cancellationToken);

            var locationResult = Location.Create(reply.Location.X, reply.Location.Y);
            if (locationResult.IsFailure)
                return GeneralErrors.InternalServerError(nameof(Client));
            return locationResult;
        }
        catch (RpcException rpcException)
        {
            return GeneralErrors.InternalServerError(rpcException.Message);
        }
    }
}